var fs = require("fs");
var request = require("request"); // npm install request --save
var Gitter = require("node-gitter"); // npm install node-gitter --save

// get config value
var data = fs.readFileSync("./config.json", "utf8"),
  config;
try {
  config = JSON.parse(data);
  console.dir(config);
} catch(e) {
  console.log("Error:", e.stack);
}
console.log("Token: " + config.gitter);
console.log("ETH: " + config.eth);
console.log("Sia: " + config.sia);

// start timer function
var eth = 0; // ETH price
var btc = 0; // BTC price
var sia = 0; // SIA price
var myr = 0; // ringgit price exchange to USD
//setInterval(updateReport, 1000*60*interval); // every x mins
sendReport();
function sendReport() {
  var now = new Date();
  console.log("Report sent at " + now);
  getCurrency();
  getPrice("BTC");
  getPrice("ETH");
  getSiaPrice();
}

function checkETHBalance() {
  var url = "https://api.ethermine.org/miner/"+config.eth+"/currentStats";
  request({
    url: url,
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      var unpaid = extractUnpaid("unpaid", body)/1e18; // in ETH unit
      unpaid = roundUp(unpaid, 10000);
      var total = unpaid*eth;
      total = roundUp(total, 100);
      var ringgit = unpaid*eth*myr;
      ringgit = roundUp(ringgit, 100);
      var message = "Unpaid: " + unpaid + "ETH=USD"+total + "=MYR"+ringgit;
      pushGitter(message);
    }
  });
}

// see https://siamining.com/api/
function checkSiaBalance() {
  var url = "https://siamining.com/api/v1/addresses/"+config.sia+"/summary";
  request({
    url: url,
    json: true
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      var unpaid = Number.parseFloat(body.paid) + Number.parseFloat(body.balance);
      var total = unpaid*sia/1000;
      total = roundUp(total, 100);
      var ringgit = unpaid*sia*myr/1000;
      ringgit = roundUp(ringgit, 100);
      var message = "Unpaid: " + roundUp(unpaid, 10000) + "SC=USD"+total+"=MYR"+ringgit;
      pushGitter(message);
    }
  });
}

// modified from https://stackoverflow.com/questions/20304862/nodejs-httpget-to-a-url-with-json-response
function getPrice(coin) {
  var url = "https://www.coinbase.com/api/v2/prices/"+coin+"-USD/spot?";
  request({
    url: url,
    json: true
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      var oldETH = eth;
      var oldBTC = btc;
      var percent = 0;
      var message = "";

      // including up/down percent
      if(coin == "ETH") {
        eth = body.data.amount; //extractPrice(body);
        var diff = eth-oldETH;
        if(oldETH > 0) {
          percent = roundUp(diff/oldETH * 100, 100);
        }
        console.log("ETH%: "+percent);
        message = "1"+coin+"=USD"+eth;
        checkETHBalance();
      } else if(coin == "BTC") {
        btc = body.data.amount; //extractPrice(body);
        var diff = btc-oldBTC;
        if(oldBTC > 0) {
          percent = roundUp(diff/oldBTC * 100, 100);
        }
        console.log("BTC%: "+percent);
        message = "1"+coin+"=USD"+btc;
      }

      // showing diff compare to last price
      if(percent > 0) {
        message += " ("+percent+"%) :arrow_up:";
      } else if(percent < 0) {
        message += " ("+percent+"%) :arrow_down:";
      }

      console.log(message);
      pushGitter(message);
    }
  });
}

/**
 * Get latest Sia Coin price.
 */
function getSiaPrice() {
  var url = "http://siapulse.com/api/exchangerate?currency=USD";
  request({
    url: url,
    json: true
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      // console.log(body);
      var oldSia = sia;
      var percent = 0;
      var message = "";
      sia = 1/body.rate*1000;
      sia = roundUp(sia, 100);
      console.log("1000SC=USD"+sia);
      message += "1000SC=USD"+sia;

      var diff = sia-oldSia;
      if(oldSia > 0) {
        percent = roundUp(diff/oldSia * 100, 100);
      }
      console.log("SC%: "+percent);

      // showing diff compare to last price
      if(percent > 0) {
        message += " ("+percent+"%) :arrow_up:";
      } else if(percent < 0) {
        message += " ("+percent+"%) :arrow_down:";
      }
      console.log(message);
      pushGitter(message);

      //checkSiaBalance();
    }
  });
}

function getCurrency() {
  var url = "https://api.fixer.io/latest?base=USD";
  request({
    url: url,
    json: true
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      myr = body.rates.MYR;
      var message = "1USD=MYR"+myr;
      console.log(message);
      pushGitter(message);
    }
  });
}

// source https://www.npmjs.com/package/node-gitter
function pushGitter(message) {

  // define gitter with sender token
  var gitter = new Gitter(config.gitter);

  // get the room id from browser url NOT the friendly name you see
  gitter.rooms.join("xinming-creative/bigtearice")
    .then(function(room){
      room.send(message);
      console.log("Message sent");
    });
}

/**
 * Return price by extract from a json string.
 * TODO: Replace with json parse method but failed.
 */
function extractPrice(body) {
  var price = 0;
  var chunks = body.split('":"');
  for(var i=0;i<chunks.length;i++) {
    //console.log(chunks[i]);
    if(chunks[i].search("amount") > -1) {
      var words = chunks[i+1].split('"');
      price = words[0];
      return price;
    }
  }
  return price;
}
/**
 * Return lookup value by extract from a json string.
 */
function extractUnpaid(lookup, body) {
  var data = 0;
  var chunks = body.split(lookup);
  var words = chunks[1].split(',');
  data = words[0].substring(2, words[0].length);
  return data;
}

function roundUp(number, precision) {
  return Math.ceil(number*precision)/precision;
}

/**
 * Append content with line break.
 */
function appendLine(content, line) {
  if(content.length > 0) {
    content += "\n";
  }
  content += line;
}
