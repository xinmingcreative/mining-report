var fs = require("fs");
var request = require("request"); // npm install request --save
var delimiter = ",";

// get config value
var data = fs.readFileSync("./config.json", "utf8"), config;
try {
  config = JSON.parse(data);
  console.dir(config);
} catch(e) {
  console.log("Error:", e.stack);
}

var now = new Date();
var raw = toShortDateString(now)+".json";

// handle different parameter
process.argv.forEach(function(val, index, array) {
  //console.log(index+": "+val);
  if(index == 2) {
    if(val == "history") {
      var url = "https://api.ethermine.org/miner/"+config.eth+"/history";
      console.log("Downloading "+url);
      // retrieve miner history from api
      getMinerHistory(url);
    } else {
      // query current balance from api
      queryBalance();
    }
  }
});


/**
 * Convert date to Excel recognized string.
 */
function toShortDateString(date) {
  var output = "";
  output += date.getFullYear() + "-";

  var month = date.getMonth()+1;
  if(month < 10) output += "0";
  output += month + "-";

  var day = date.getDate();
  if(day < 10) output += "0";
  output += day;

  return output;
}

/**
 * Convert date to Excel recognized string.
 */
function toLongDateString(date) {
  var output = "";
  output += toShortDateString(date) + " ";

  var hours = date.getHours();
  if(hours < 10) output += "0";
  output += hours + ":";

  var minutes = date.getMinutes();
  if(minutes < 10) output += "0";
  output += minutes + ":";

  var seconds = date.getSeconds();
  if(seconds < 10) output += "0";
  output += seconds;

  return output;
}

/**
 * Return joined string with comma delimiter.
 */
function getRow(data) {
  var row = "";

  var date = new Date(data.time*1000);
  row += toLongDateString(date);
  row += delimiter+data.time;
  row += delimiter+data.reportedHashrate;
  row += delimiter+data.currentHashrate;
  row += delimiter+data.validShares;
  row += delimiter+data.invalidShares;
  row += delimiter+data.staleShares;
  row += delimiter+data.averageHashrate;
  row += delimiter+data.activeWorkers;

  return row;
}

/**
 * Retrieve miner history from API call.
 */
function getMinerHistory(url) {
  request({
    url: url,
    json: true
  }, function(error, response, body) {

    if(!error && response.statusCode == 200) {

      // write header to csv file
      var now = new Date();
      var fileName = toShortDateString(now)+".csv";
      var header = "date,time,reportedHashrate,currentHashrate,validShares,invalidShares,staleShares,averageHashrate,activeWorkers";
      fs.appendFileSync(fileName, header+"\n");

      // dump records
      for(var i=0;i<body.data.length;i++) {
        var data = body.data[i];
        console.log(data.time+","+data.validShares);
        fs.appendFile(fileName, getRow(data)+"\n");
      }

    } else if(!error) {
      console.log(error);
    }
  }).pipe(fs.createWriteStream(raw));
}

/**
 * Query account balance from pool.
 */
function queryBalance() {
  var url = "https://api.ethermine.org/miner/"+config.eth+"/currentStats";
  request({
    url: url,
    json: true
  }, function(error, response, body) {

    if(!error && response.statusCode == 200) {

      var fileName = "balance.csv";
      fs.stat(fileName, function(err, stat) {
        if(err == null) {
          // File exists
        } else if(err.code == 'ENOENT') {
          // file does not exist write header to csv file
          var header = "date,time,lastSeen,reportedHashrate,currentHashrate,validShares,invalidShares,staleShares,averageHashrate,activeWorkers,unpaid,unconfirmed,coinsPerMin,usdPerMin,btcPerMin";
          fs.appendFileSync(fileName, header+"\n");
        } else {
          console.log('Some other error: ', err.code);
        }
      });

      var row = "";
      var date = new Date(body.data.time*1000);
      row += toLongDateString(date);
      row += delimiter+body.data.time;
      row += delimiter+body.data.lastSeen;
      row += delimiter+body.data.reportedHashrate;
      row += delimiter+body.data.currentHashrate;
      row += delimiter+body.data.validShares;
      row += delimiter+body.data.invalidShares;
      row += delimiter+body.data.staleShares;
      row += delimiter+body.data.averageHashrate;
      row += delimiter+body.data.activeWorkers;
      row += delimiter+body.data.unpaid;
      row += delimiter+body.data.unconfirmed;
      row += delimiter+body.data.coinsPerMin;
      row += delimiter+body.data.usdPerMin;
      row += delimiter+body.data.btcPerMin;
      fs.appendFile(fileName, row+"\n");
      console.log(body.data.time+":"+body.data.unpaid);

    } else if(!error) {
      console.log(error);
    }
  });
}
