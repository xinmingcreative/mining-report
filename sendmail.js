var fs = require("fs");
var nodemailer = require("nodemailer"); // npm install nodemailer --save
var request = require("request"); // npm install request --save

// get config value
var data = fs.readFileSync("./config.json", "utf8"),
  config;
try {
  config = JSON.parse(data);
  console.dir(config);
} catch(e) {
  console.log("Error:", e.stack);
}
console.log("Email: " + config.email);

var myr = 0; // ringgit price exchange to USD
var eth = 0; // ETH price in USD
var btc = 0; // BTC pricd in USD
var sum = 0; // all time sum of ETH unit

// generate report
var report = "";
getCurrency();

/**
 * Convert date to Excel recognized string.
 */
function toShortDateString(date) {
  var output = "";
  output += date.getFullYear() + "-";

  var month = date.getMonth()+1;
  if(month < 10) output += "0";
  output += month + "-";

  var day = date.getDate();
  if(day < 10) output += "0";
  output += day;

  return output;
}

/**
 * Return epoch timestamp of midnight.
 */
function getMidnight() {
  var now = new Date();
  var todayMidnight = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0);
  console.log("Now: " + now);
  console.log("Midnight: " +todayMidnight);

  var midnight = Date.parse(todayMidnight)/1000;
  console.log("Epoch: " + midnight);
  return midnight;
}

/**
 * Return epoch timestamp of yesterday.
 */
function getYesterday() {
  var now = new Date();
  var midnight = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0);
  console.log("Midnight: " + midnight);

  var epoch = Date.parse(midnight)/1000;
  epoch -= 60*24;
  var yesterday = new Date(epoch*1000);
  console.log("Yesterday: " + yesterday);
  return epoch;
}

function roundUp(number, precision) {
  return Math.ceil(number*precision)/precision;
}

function getCurrency() {
  var url = "http://www.apilayer.net/api/live?access_key=45def0751b4af66b18366f7dd422bf80";
  request({
    url: url,
    json: true
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      myr = body.quotes.USDMYR;
      console.log("1USD=MYR"+myr);
      getPrice("BTC");
    }
  });
}

// modified from https://stackoverflow.com/questions/20304862/nodejs-httpget-to-a-url-with-json-response
function getPrice(coin) {
  var url = "https://www.coinbase.com/api/v2/prices/"+coin+"-USD/spot?";
  request({
    url: url,
    json: true
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      //report += "\n1"+coin+"=USD"+body.data.amount;
      if(coin == "BTC") {
        btc = body.data.amount;
        getPrice("ETH");
      } else if(coin == "ETH") {
        eth = body.data.amount;
        getETHPayouts();
      }
    }
  });
}

/**
 * Sum all payouts all time.
 */
function getETHPayouts() {
  var url = "https://api.ethermine.org/miner/"+config.eth+"/payouts";
  request({
    url: url,
    json: true,
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      for(var i=0;i<body.data.length;i++) {
        var data = body.data[i];
        console.log(data.paidOn+": " + data.amount);
        var amount = data.amount/1e18;
        amouunt = roundUp(amount, 100000);
        sum += amount;
      }
      console.log("All payouts: " + sum+"ETH");
      getETHBalance();
    }
  });
}

/**
 * Get unpaid balance from ethermine.org pool.
 */
function getETHBalance() {
  var url = "https://api.ethermine.org/miner/"+config.eth+"/currentStats";
  request({
    url: url,
    json: true
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {

      var minCoins = body.data.coinsPerMin;
      var dailyCoins = 0;
      if(typeof body.data.lastSeen !== "undefined") {
        var lastSeen = new Date(body.data.lastSeen*1000);
        console.log("Last Seen:"+lastSeen);

        var totalMin = (body.data.lastSeen-getYesterday())/60;
        // make sure the range is [0, 1440]
        totalMin = Math.max(24*60, Math.max(0,totalMin));
        console.log("Total min: " + totalMin);
        dailyCoins = totalMin*minCoins;
        console.log("Daily Earn: " + dailyCoins);
      }

      var harvest = 1/dailyCoins;
      dailyCoins = roundUp(dailyCoins, 100000);
      harvest = roundUp(harvest, 100000);

      var total = dailyCoins*eth;
      total = roundUp(total, 100);
      var ringgit = dailyCoins*eth*myr;
      ringgit = roundUp(ringgit, 100);

      report += "\nDaily: "+dailyCoins + "ETH=USD"+total+"=MYR"+ringgit;
      report += "\nActive Workers: " + body.data.activeWorkers
      report += "\nHarvest: " + harvest+" days";

      console.log("Unpaid: " + body.data.unpaid);
      var unpaid = body.data.unpaid/1e18; // in ETH unit
      unpaid = roundUp(unpaid, 100000);
      sum += unpaid;
      sum = roundUp(sum, 100000);
      report += "\nAll time: "+sum+"ETH";
      report += "\nUnpaid: " +unpaid +"ETH";

      report += "\n\n1USD=MYR"+myr;
      report += "\n1BTC=USD"+btc;
      report += "\n1ETH=USD"+eth;

      var ratio = btc/eth;
      ratio = roundUp(ratio, 100000);
      report += "\n1BTC="+ratio+"ETH";
      report += "\n\nSummary of "+config.eth;

      sendMail();
    }
  });
}

// see https://nodemailer.com/about/
function sendMail() {
  var transporter = nodemailer.createTransport({
    host: config.host,
    port: 587,
    secure: false,
    auth: {
      user: config.email,
      pass: config.password
    }
  });
  var now = new Date();
  var mailOptions = {
    from: config.email,
    to: config.email,
    subject: "Daily Mining Report: "+toShortDateString(now),
    text: report
  };
  console.log("Start sending email");
  transporter.sendMail(mailOptions, function(error, info){
    if(error) {
      console.log(error);
    } else {
      console.log("Email sent: "+info.response);
    }
  });
}
