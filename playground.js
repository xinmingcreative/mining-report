// git rev-parse HEAD
var process = require("child_process");

try {
  var now = new Date();
  var midnight = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0);
  console.log("Midnight: " + midnight);

  var epoch = Date.parse(midnight)/1000;
  epoch -= 60*24;
  var yesterday = new Date(epoch*1000);
  console.log("Yesterday: " + yesterday);
} catch(err) {
  console.log(err);
}

try {
  var gitLastLog = process.execSync("git log -1").toString().trim();
  console.log(gitLastLog);
  var info = parseGitLog(gitLastLog);
  console.log("version: "+info[0]);
  console.log("date: "+info[1]);
} catch(err) {
  console.log(err);
}

/**
 * Parse result from git log into array
 * where [0] is git hash and [1] is a date.
 */
function parseGitLog(result) {
  var info = [];
  var lines = result.split("\n");
  var words = lines[0].split(" ");
  info[0] = words[1].trim().substring(0,6);

  //var date = words[5]+"-"+words[2]+"-"+words[3];
  var date = lines[2].substring(6, lines[2].length-6);
  console.log(date.trim());
  info[1] = date.trim();

  return info;
}

var request = require("request");
var eth = getPrice("ETH", function(error, price){
  if(error) {
    console.log(error);
    return;
  }
  console.log(price);
  return price;
});
var btc = getPrice("BTC", function(error, price){
  if(error) {
    console.log(error);
    return;
  }
  console.log(price);
  return price;
});
var ltc = getPrice("LTC", function(error, price){
  if(error) {
    console.log(error);
    return;
  }
  console.log(price);
  return price;
});
var sia = getSiaPrice();

console.log("ETH:"+eth);
console.log("BTC:"+btc);
console.log("LTC:"+ltc);
console.log("SIA:"+sia);
getCurrency();

// BTC, ETH, LTC
function getPrice(coin, callback) {
  var callbackInvoked = false;
  var url = "https://www.coinbase.com/api/v2/prices/"+coin+"-USD/spot?";
  request({
    url: url,
    json: true
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      // console.log(body);
      var price = body.data.amount; // extractPrice(body);
      console.log(price);
      if(!callbackInvoked && (error || price > 0)) {
        if(error) {
          callback(error, null);
        } else {
          callback(null, price); // extractPrice(body));
        }
        callbackInvoked = true;
      }

      // switch(coin) {
      //   case "BTC": btc = price;break;
      //   case "ETH": eth = price;break;
      //   case "LTC": ltc = price;break;
      // }
    }
  });
}

function getCurrency() {
  var url = "https://api.fixer.io/latest?base=USD";
  request({
    url: url,
    json: true
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      console.log("1USD=MYR"+body.rates.MYR);
    }
  });
}

/**
 * Return price by extract from a json string.
 * TODO: Replace with json parse method but failed.
 */
function extractPrice(body) {
  var price = 0;
  var chunks = body.split('":"');
  for(var i=0;i<chunks.length;i++) {
    //console.log(chunks[i]);
    if(chunks[i].search("amount") > -1) {
      var words = chunks[i+1].split('"');
      price = words[0];
      return price;
    }
  }
  return price;
}

/**
 * Return lookup value by extract from a json string.
 */
function extractUnpaid(lookup, body) {
  var data = 0;
  var chunks = body.split(lookup);
  var words = chunks[1].split(',');
  data = words[0].substring(2, words[0].length);
  return data;
}

function roundUp(number, precision) {
  return Math.ceil(number*precision)/precision;
}

function getSiaPrice() {
  var url = "http://siapulse.com/api/exchangerate?currency=USD";
  request({
    url: url,
    json: true
  }, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      // console.log(body);
      var price = 1/body.rate * 1000;
      console.log("1000SIA=USD"+price);
    }
  });
}
