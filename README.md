# Mining Reports
A collection of useful reports and tools for mining uses.

## Requirements
- [NodeJs](https://nodejs.org/en/)

## Before Start
1. ``npm install request --save``
2. Create ``config.json``.

[config.json]
```json
{
	"eth": "ETH_account",
	"host": "smtp.gmail.com",
	"email": "your@gmail.com",
	"password": "email_password"
}
```

## First Time
1. ``git clone https://gitlab.com/xinmingcreative/mining-report.git``
2. Start ``snapshot.bat``.

## Update Latest Script
1. ``git pull``
2. Start ``snapshot.bat``.

## Commands
1. Snapshot Miner History - ``snapshot.bat``.
2. Snapshot Miner Balance - ``query-balance.bat``.

## Alternative Wallet beside Paypal
1. https://ko-fi.com/
2. https://liberapay.com/

## References
- [Node.js](http://nodejs.org)
- https://github.com/request/request
- [Javascript Date Functions](https://www.w3schools.com/jsref/jsref_obj_date.asp)
- [node-gitter](https://www.npmjs.com/package/node-gitter)
- https://api.ethermine.org/docs/#api-Miner-miner_currentStats
- https://api.ethermine.org/docs/#api-Miner-miner_historicStats
